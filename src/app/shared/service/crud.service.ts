import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { ENDPOINTS, ENDPOINT_CONSTRUCTOR_NAME } from '../consts';
import { EntityMapper } from '../model/classes/entity-mapper.class';
import { EntityRegister } from '../model/classes/entity-register.class';

@Injectable({ providedIn: 'root' })
export class CRUDService {
  // public
  public dataBehaviorSubject$: BehaviorSubject<{ [key: string]: any[] }> =
    new BehaviorSubject<{ [key: string]: any[] }>({});

  // private
  private _data: { [key: string]: any[] } = {};
  private _entityRegister: EntityRegister;

  constructor(
    private _httpClient: HttpClient,
    private _entityMapper: EntityMapper
  ) {
    this._entityRegister = EntityRegister.getInstance();

    ENDPOINTS.forEach((endpoint) => {
      this._httpClient
        .get<any[]>(`http://localhost:3000/${endpoint}`)
        .pipe(
          take(1),
          tap((data) => {
            this._data[endpoint] = data.map((doc) =>
              this._entityMapper.toEntity(doc)
            );
            this.dataBehaviorSubject$.next(this._data);
          })
        )
        .subscribe();
    });
  }

  public get = (key: string): any[] => {
    return this._data[key];
  };

  public post = (entity: { _id: string }, endpoint: string): void => {
    this._httpClient
      .post<{ _id: string }>(`http://localhost:3000/${endpoint}`, {
        ...this._entityMapper.toDoc(entity),
        _$constructorName: ENDPOINT_CONSTRUCTOR_NAME[endpoint],
      })
      .pipe(take(1))
      .subscribe((doc) => {
        console.log(doc);

        let entity = this._entityMapper.toEntity(doc as any);
        this._data[endpoint].push(entity);
        this.dataBehaviorSubject$.next(this._data);
      });
  };

  public put = (entity: { _id: string }, endpoint: string): void => {
    this._httpClient
      .put(`http://localhost:3000/${endpoint}/${entity._id}`, {
        ...this._entityMapper.toDoc(entity),
        _$constructorName: ENDPOINT_CONSTRUCTOR_NAME[endpoint],
      })
      .pipe(take(1))
      .subscribe((data) => {
        // console.log(data);
      });
  };

  public delete = (entity: { _id: string }, endpoint: string): void => {
    this._httpClient
      .delete(`http://localhost:3000/${endpoint}/${entity._id}`)
      .pipe(take(1))
      .subscribe(() => {
        this._data[endpoint] = this._data[endpoint].filter(
          (_entity) => _entity._id !== entity._id
        );
        this.dataBehaviorSubject$.next(this._data);
        this._entityRegister.unregister(entity);
      });
  };
}
