export class EntityRegister {
  private static _instance: EntityRegister;

  private static _observers: { [key: string]: Function[] } = {};
  private static _deletedObervableKeys: string[] = [];

  private _entities: { [key: string]: any } = {};

  private constructor() {}

  public clear = (): void => {
    EntityRegister._observers = {};
    EntityRegister._deletedObervableKeys = [];

    this._entities = {};
  };

  public static getInstance = (): EntityRegister => {
    if (!EntityRegister._instance)
      EntityRegister._instance = new EntityRegister();
    return EntityRegister._instance;
  };

  public register = (entity: { _id: string }): any => {
    if (!this._entities.hasOwnProperty(entity._id)) {
      // entity not registered
      this._entities[entity._id] = entity;

      if (EntityRegister._observers[entity._id]) {
        EntityRegister._observers[entity._id].forEach((observer) =>
          observer(entity)
        );

        EntityRegister._observers[entity._id] = [];
        delete EntityRegister._observers[entity._id];
        EntityRegister._deletedObervableKeys.push(entity._id);
      }
    }

    // console.log(EntityRegister._observers);

    return this._entities[entity._id];
  };

  public unregister = (entity: { _id: string }): any => {
    console.log(entity, 'unregister');

    if (this._entities.hasOwnProperty(entity._id))
      delete this._entities[entity._id];

    return entity;
  };

  public getEntity = (id: string): any => {
    // console.log(this._entities);
    return { ...this._entities }[id];
  };

  // observable fns
  /**
   * detach observer-a ce se izvrsiti prilikom registracije entiteta ciji je _id observable
   */
  public attach = (_id: string, observer: (entity: any) => void): void => {
    if (EntityRegister._deletedObervableKeys.includes(_id))
      throw new Error(`_id: ${_id}, already existed as observable.`);

    if (!(_id in EntityRegister._observers))
      EntityRegister._observers[_id] = [];

    EntityRegister._observers[_id].push(observer);
  };
}
