import { Karta } from './karta.class';

export class Putnik {
    // attrs
    private _id: string | undefined = undefined;
    private ime: string | undefined = undefined;
    private prezime: string | undefined = undefined;
    private __karte: Karta[] = [ ];
    
    // getters & setters
    public get_id (): string | undefined { return this._id; }
    public set_id (_id: string | undefined): void { this._id = _id; }
    
    public getIme (): string | undefined { return this.ime; }
    public setIme (ime: string | undefined): void { this.ime = ime; }
    
    public getPrezime (): string | undefined { return this.prezime; }
    public setPrezime (prezime: string | undefined): void { this.prezime = prezime; }
    
    public get__karte (): Karta[] { return this.__karte; }
    public set__karte (__karte: Karta[]): void { this.__karte = __karte; }
    
    // fns
    
}