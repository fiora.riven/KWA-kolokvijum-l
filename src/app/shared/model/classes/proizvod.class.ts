import { KupacProizvod } from './kupac-proizvod.class';

export class Proizvod {
  // attrs
  private _id: string | undefined = undefined;
  private naziv: string | undefined = undefined;
  private opis: string | undefined = undefined;
  private slika: string | undefined = undefined;
  private __proizvodKupci: KupacProizvod[] = [];

  // getters & setters
  public get_id(): string | undefined {
    return this._id;
  }
  public set_id(_id: string | undefined): void {
    this._id = _id;
  }

  public getNaziv(): string | undefined {
    return this.naziv;
  }
  public setNaziv(naziv: string | undefined): void {
    this.naziv = naziv;
  }

  public getOpis(): string | undefined {
    return this.opis;
  }
  public setOpis(opis: string | undefined): void {
    this.opis = opis;
  }

  public getSlika(): string | undefined {
    return this.slika;
  }
  public setSlika(slika: string | undefined): void {
    this.slika = slika;
  }

  public get__proizvodKupci(): KupacProizvod[] {
    return this.__proizvodKupci;
  }
  public set__proizvodKupci(__proizvodKupci: KupacProizvod[]): void {
    this.__proizvodKupci = __proizvodKupci;
  }

  // fns
}
