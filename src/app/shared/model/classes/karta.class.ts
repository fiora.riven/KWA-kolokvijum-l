import { Stanica } from './stanica.class';
import { Putnik } from './putnik.class';

export class Karta {
  // attrs
  private _id: string | undefined = undefined;
  private datumVremePolaska: Date | undefined = new Date();
  private prtljag: boolean = false;
  private cena: number = 100;
  private __stanica: Stanica | undefined = undefined;
  private __putnik: Putnik | undefined = undefined;

  // getters & setters
  public get_id(): string | undefined {
    return this._id;
  }
  public set_id(_id: string | undefined): void {
    this._id = _id;
  }

  public getDatumVremePolaska(): Date | undefined {
    return this.datumVremePolaska;
  }
  public setDatumVremePolaska(datumVremePolaska: Date | undefined): void {
    this.datumVremePolaska = datumVremePolaska;
  }

  public getPrtljag(): boolean {
    return this.prtljag;
  }
  public setPrtljag(prtljag: boolean): void {
    this.prtljag = prtljag;
  }

  public getCena(): number {
    return this.cena;
  }
  public setCena(cena: number): void {
    this.cena = cena;
  }

  public get__stanica(): Stanica | undefined {
    return this.__stanica;
  }
  public set__stanica(__stanica: Stanica | undefined): void {
    this.__stanica = __stanica;
  }

  public get__putnik(): Putnik | undefined {
    return this.__putnik;
  }
  public set__putnik(__putnik: Putnik | undefined): void {
    this.__putnik = __putnik;
  }

  // fns
}
