import { Injectable, NgZone } from '@angular/core';
import { firstLetterUp } from '../../helper-fns';
import { EntityRegister } from './entity-register.class';

import * as moment from 'moment';
import { Karta } from './karta.class';
import { Putnik } from './putnik.class';
import { Stanica } from './stanica.class';
import { Kupac } from './kupac.class';
import { KupacProizvod } from './kupac-proizvod.class';
import { Proizvod } from './proizvod.class';

// classes

@Injectable({ providedIn: 'root' })
export class EntityMapper {
  private modelConstructors: { [key: string]: any } = {
    Karta: Karta,
    Putnik: Putnik,
    Stanica: Stanica,
    Kupac: Kupac,
    KupacProizvod: KupacProizvod,
    Proizvod: Proizvod,
  };

  constructor(private _ngZone: NgZone) {}

  public toEntity<T>(doc: {
    _id: string;
    _$constructorName: string;
    [key: string]: any;
  }): T {
    let register: EntityRegister = EntityRegister.getInstance();
    /**
     * check register
     */
    let entity: any = register.getEntity(doc._id);
    if (entity) {
      // console.log('found');
      return entity;
    }

    if (!doc['_$constructorName'])
      throw Error("Doc doesn't have constructor name.");

    entity = new this.modelConstructors[doc['_$constructorName']]();
    try {
      for (let attrName in doc) {
        if (attrName === '_$constructorName') continue;
        let data = attrName.split('_');
        let attrValue = doc[attrName];

        let methodName: string;
        let primitive: boolean = true;
        if (data[0] === '' && data[1] === '') {
          // non-primitive data type
          primitive = false;
          data = [data[2], ...data.slice(3).map((word) => firstLetterUp(word))];
          methodName = data.reduce((acc, value) => acc + value, '__');

          if (attrValue) {
            /**
             * check register
             */
            let _entity: any;
            if (typeof attrValue === 'string' && attrValue.length === 24) {
              let _id: string = attrValue;
              _entity = register.getEntity(_id) || attrValue;
            } else if (Array.isArray(attrValue)) {
              attrValue = attrValue.map((entity) => {
                if (typeof entity === 'string' && entity.length === 24)
                  _entity = register.getEntity(entity) || entity;
                else if (typeof entity === 'object' && '_id' in entity) {
                  _entity = register.getEntity(entity['_id']);

                  if (!_entity && entity['_$constructorName'])
                    _entity = this.toEntity(entity);
                }

                return _entity;
              });
              _entity = attrValue;
            } else if (typeof attrValue === 'object' && '_id' in attrValue) {
              _entity = register.getEntity(attrValue['_id']);
            }

            if (_entity) attrValue = _entity;
            else if (attrValue['_$constructorName']) {
              attrValue = this.toEntity(attrValue);
            } else attrValue = undefined;
          }
        } else if (data[0] === '') {
          // primitive data type
          data = [data[1], ...data.slice(2).map((word) => firstLetterUp(word))];
          methodName = data.reduce((acc, value) => acc + value, '_');
        } else {
          // primitive data type
          data = data.map((word) => firstLetterUp(word));
          methodName = data.reduce((acc, value) => acc + value, '');
        }
        let getMethodName: string = 'get' + methodName;
        let setMethodName: string = 'set' + methodName;

        if (moment(attrValue, moment.ISO_8601).isValid())
          attrValue = new Date(attrValue);

        /**
         * Attach observers
         */
        if (!primitive) {
          if (typeof attrValue === 'string' && attrValue.length === 24) {
            let _id: string = attrValue;
            // console.log('_id', _id);
            this._ngZone.runOutsideAngular(() => {
              register.attach(_id, (observable) => {
                entity[setMethodName](observable);
              });
            });
          } else if (Array.isArray(attrValue)) {
            this._ngZone.runOutsideAngular(() => {
              attrValue.forEach((e: any) => {
                if (typeof e === 'string' && e.length === 24) {
                  let _id: string = e;
                  // console.log('_id', _id);
                  register.attach(_id, (observable: any) => {
                    if ((entity[getMethodName]() as string[]).includes(_id))
                      entity[setMethodName](
                        (entity[getMethodName]() as []).map((e) =>
                          e === _id ? observable : e
                        )
                      );
                  });
                }
              });
            });
          }
        }

        if (entity[setMethodName]) {
          entity[setMethodName](attrValue);
        } else {
          console.log(
            `${entity.constructor.name} doesn't have a method: ${setMethodName}`
          );
        }
      }
    } catch (error) {
      throw error;
    }

    // register entity
    register.register(entity);

    return entity;
  }

  public toDoc = (entity: any): any => {
    let doc: any = {};
    Object.getOwnPropertyNames(entity).forEach((attrName) => {
      if (entity[attrName] !== undefined)
        if (Array.isArray(entity[attrName])) {
          doc[attrName] = (entity[attrName] as any[]).map((attrValue) => {
            if (typeof attrValue === 'string' && attrValue.length === 24)
              return attrValue;
            return attrValue['_id'];
          });
        } else {
          if (attrName.startsWith('__'))
            doc[attrName] = entity[attrName]['_id'];
          else doc[attrName] = entity[attrName];
        }
    });
    return doc;
  };
}
