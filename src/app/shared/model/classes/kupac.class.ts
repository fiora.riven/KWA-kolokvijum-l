import { KupacProizvod } from './kupac-proizvod.class';

export class Kupac {
    // attrs
    private _id: string | undefined = undefined;
    private ime: string | undefined = undefined;
    private prezime: string | undefined = undefined;
    private procentualniPopustNaProizvode: number = 0;
    private __kupacProizvodi: KupacProizvod[] = [ ];
    
    // getters & setters
    public get_id (): string | undefined { return this._id; }
    public set_id (_id: string | undefined): void { this._id = _id; }
    
    public getIme (): string | undefined { return this.ime; }
    public setIme (ime: string | undefined): void { this.ime = ime; }
    
    public getPrezime (): string | undefined { return this.prezime; }
    public setPrezime (prezime: string | undefined): void { this.prezime = prezime; }
    
    public getProcentualniPopustNaProizvode (): number { return this.procentualniPopustNaProizvode; }
    public setProcentualniPopustNaProizvode (procentualniPopustNaProizvode: number): void { this.procentualniPopustNaProizvode = procentualniPopustNaProizvode; }
    
    public get__kupacProizvodi (): KupacProizvod[] { return this.__kupacProizvodi; }
    public set__kupacProizvodi (__kupacProizvodi: KupacProizvod[]): void { this.__kupacProizvodi = __kupacProizvodi; }
    
    // fns
    
}