import { Kupac } from './kupac.class';
import { Proizvod } from './proizvod.class';

export class KupacProizvod {
  // attrs
  private _id: string | undefined = undefined;
  private __kupac: Kupac | undefined = undefined;
  private __proizvod: Proizvod | undefined = undefined;

  // getters & setters
  public get_id(): string | undefined {
    return this._id;
  }
  public set_id(_id: string | undefined): void {
    this._id = _id;
  }

  public get__kupac(): Kupac | undefined {
    return this.__kupac;
  }
  public set__kupac(__kupac: Kupac | undefined): void {
    this.__kupac = __kupac;
  }

  public get__proizvod(): Proizvod | undefined {
    return this.__proizvod;
  }
  public set__proizvod(__proizvod: Proizvod | undefined): void {
    this.__proizvod = __proizvod;
  }

  // fns
}
