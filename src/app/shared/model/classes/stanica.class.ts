import { Karta } from './karta.class';

export class Stanica {
  // attrs
  private _id: string | undefined = undefined;
  private naziv: string | undefined = undefined;
  private adresa: string | undefined = undefined;
  private __karte: Karta[] = [];

  // getters & setters
  public get_id(): string | undefined {
    return this._id;
  }
  public set_id(_id: string | undefined): void {
    this._id = _id;
  }

  public getNaziv(): string | undefined {
    return this.naziv;
  }
  public setNaziv(naziv: string | undefined): void {
    this.naziv = naziv;
  }

  public getAdresa(): string | undefined {
    return this.adresa;
  }
  public setAdresa(adresa: string | undefined): void {
    this.adresa = adresa;
  }

  public get__karte(): Karta[] {
    return this.__karte;
  }
  public set__karte(__karte: Karta[]): void {
    this.__karte = __karte;
  }

  // fns
}
