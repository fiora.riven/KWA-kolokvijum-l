import { CRUDService } from './service/crud.service';
import { EntityRegister } from './model/classes/entity-register.class';
import { Karta } from './model/classes/karta.class';
import { Putnik } from './model/classes/putnik.class';
import { Stanica } from './model/classes/stanica.class';
import { Proizvod } from './model/classes/proizvod.class';
import { KupacProizvod } from './model/classes/kupac-proizvod.class';
import { Kupac } from './model/classes/kupac.class';

export const ENDPOINTS: string[] = [
  'putnici',
  'karte',
  'stanice',
  'kupci',
  'kupacproizvodi',
  'proizvodi',
];
export const ENDPOINT_CONSTRUCTOR_NAME: { [key: string]: string } = {
  putnici: 'Putnik',
  karte: 'Karta',
  stanice: 'Stanica',
  kupci: 'Kupac',
  kupacproizvodi: 'KupacProizvod',
  proizvodi: 'Proizvod',
};

export type DOC_RECIPES_TYPE = {
  [key: string]: {
    [key: string]: {
      cellHTMLElement:
        | 'INPUT-TEXT'
        | 'INPUT-NUMBER'
        | 'INPUT-DATE'
        | 'INPUT-TIME'
        | 'TEXTAREA'
        | 'SELECT'
        | 'LIST';
      getValue: (doc: any) => any;
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => void;
      getOptions: (data: { [key: string]: any[] }) => any[];
    };
  };
};

export const DOC_RECIPES: DOC_RECIPES_TYPE = {
  putnici: {
    _id: {
      cellHTMLElement: 'INPUT-TEXT',
      getValue: (doc) => doc._id,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = value;
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'putnici');
      },
      getOptions: (data) => [],
    },
    ime: {
      cellHTMLElement: 'INPUT-TEXT',
      getValue: (doc) => doc.ime,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = value;
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'putnici');
      },
      getOptions: (data) => [],
    },
    prezime: {
      cellHTMLElement: 'INPUT-TEXT',
      getValue: (doc) => doc.prezime,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = value;
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'putnici');
      },
      getOptions: (data) => [],
    },
    __karte: {
      cellHTMLElement: 'LIST',
      getValue: (doc) => doc.__karte,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {},
      getOptions: (data) => [],
    },
  },
  karte: {
    _id: {
      cellHTMLElement: 'INPUT-TEXT',
      getValue: (doc) => doc._id,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = value;
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'karte');
      },
      getOptions: (data) => [],
    },
    cena: {
      cellHTMLElement: 'INPUT-NUMBER',
      getValue: (doc) => doc.cena,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = +value;
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'karte');
      },
      getOptions: (data) => [],
    },
    datumVremePolaska: {
      cellHTMLElement: 'INPUT-DATE',
      getValue: (doc) => {
        let d = new Date(doc.datumVremePolaska),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
      },
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = new Date(value);
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'karte');
      },
      getOptions: (data) => [],
    },
    prtljag: {
      cellHTMLElement: 'SELECT',
      getValue: (doc) => doc.prtljag,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = value === 'false' ? false : true;
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'karte');
      },
      getOptions: (data: { [key: string]: any[] }) => {
        return [true, false];
      },
    },
    __putnik: {
      cellHTMLElement: 'SELECT',
      getValue: (doc) => doc.__putnik,
      setValue: (
        value: any,
        tableData: { entity: Karta; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        let noviPutnikId: string = value;
        let noviPutnik: Putnik = entityRegister.getEntity(noviPutnikId);

        let stariPutnik = tableData.entity.get__putnik();

        tableData.doc[attrName] = noviPutnikId;
        tableData.entity.set__putnik(noviPutnik);

        if (stariPutnik)
          stariPutnik.set__karte(
            stariPutnik
              .get__karte()
              .filter((karta) => karta.get_id() !== tableData.entity.get_id())
          );
        noviPutnik.get__karte().push(tableData.entity);

        console.log('update', tableData);
        if (tableData.entity) CRUDService.put(tableData.entity as any, 'karte');
        if (noviPutnik) CRUDService.put(noviPutnik as any, 'putnici');
        if (stariPutnik) CRUDService.put(stariPutnik as any, 'putnici');
      },
      getOptions: (data: { [key: string]: any[] }) => {
        if (data) return data['putnici'].map((putnik) => putnik._id);
        return [];
      },
    },
    __stanica: {
      cellHTMLElement: 'SELECT',
      getValue: (doc) => doc.__stanica,
      setValue: (
        value: any,
        tableData: { entity: Karta; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        let novaStanicaId: string = value;
        let novaStanica: Stanica = entityRegister.getEntity(novaStanicaId);

        let staraStanica = tableData.entity.get__stanica();

        tableData.doc[attrName] = novaStanicaId;
        tableData.entity.set__stanica(novaStanica);

        if (staraStanica)
          staraStanica.set__karte(
            staraStanica
              .get__karte()
              .filter((karta) => karta.get_id() !== tableData.entity.get_id())
          );
        novaStanica.get__karte().push(tableData.entity);

        console.log('update', tableData);
        if (tableData.entity) CRUDService.put(tableData.entity as any, 'karte');
        if (novaStanica) CRUDService.put(novaStanica as any, 'stanice');
        if (staraStanica) CRUDService.put(staraStanica as any, 'stanice');
      },
      getOptions: (data: { [key: string]: any[] }) => {
        if (data) return data['stanice'].map((stanica) => stanica._id);
        return [];
      },
    },
  },
  stanice: {
    _id: {
      cellHTMLElement: 'INPUT-TEXT',
      getValue: (doc) => doc._id,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = value;
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'stanice');
      },
      getOptions: (data) => [],
    },
    adresa: {
      cellHTMLElement: 'INPUT-TEXT',
      getValue: (doc) => doc.adresa,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = value;
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'stanice');
      },
      getOptions: (data) => [],
    },
    naziv: {
      cellHTMLElement: 'INPUT-TEXT',
      getValue: (doc) => doc.naziv,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = value;
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'stanice');
      },
      getOptions: (data) => [],
    },
    __karte: {
      cellHTMLElement: 'LIST',
      getValue: (doc) => doc.__karte,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {},
      getOptions: (data) => [],
    },
  },
  kupci: {
    _id: {
      cellHTMLElement: 'INPUT-TEXT',
      getValue: (doc) => doc._id,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = value;
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'kupci');
      },
      getOptions: (data) => [],
    },
    ime: {
      cellHTMLElement: 'INPUT-TEXT',
      getValue: (doc) => doc.ime,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = value;
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'kupci');
      },
      getOptions: (data) => [],
    },
    prezime: {
      cellHTMLElement: 'INPUT-TEXT',
      getValue: (doc) => doc.prezime,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = value;
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'kupci');
      },
      getOptions: (data) => [],
    },
    procentualniPopustNaProizvode: {
      cellHTMLElement: 'INPUT-NUMBER',
      getValue: (doc) => doc.procentualniPopustNaProizvode,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = +value;
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'kupci');
      },
      getOptions: (data) => [],
    },
    __kupacProizvodi: {
      cellHTMLElement: 'LIST',
      getValue: (doc) => doc.__kupacProizvodi,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {},
      getOptions: (data) => [],
    },
  },
  kupacproizvodi: {
    _id: {
      cellHTMLElement: 'INPUT-TEXT',
      getValue: (doc) => doc._id,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = value;
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'kupacproizvodi');
      },
      getOptions: (data) => [],
    },

    __kupac: {
      cellHTMLElement: 'SELECT',
      getValue: (doc) => doc.__kupac,
      setValue: (
        value: any,
        tableData: { entity: KupacProizvod; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        let noviKupacId: string = value;
        let noviKupac: Kupac = entityRegister.getEntity(noviKupacId);

        let stariKupac = tableData.entity.get__kupac();

        tableData.doc[attrName] = noviKupacId;
        tableData.entity.set__kupac(noviKupac);

        if (stariKupac)
          stariKupac.set__kupacProizvodi(
            stariKupac
              .get__kupacProizvodi()
              .filter(
                (kupacProizvod) =>
                  kupacProizvod.get_id() !== tableData.entity.get_id()
              )
          );
        noviKupac.get__kupacProizvodi().push(tableData.entity);

        console.log('update', tableData);
        if (tableData.entity)
          CRUDService.put(tableData.entity as any, 'kupacproizvodi');
        if (noviKupac) CRUDService.put(noviKupac as any, 'kupci');
        if (stariKupac) CRUDService.put(stariKupac as any, 'kupci');
      },
      getOptions: (data: { [key: string]: any[] }) => {
        if (data) return data['kupci'].map((putnik) => putnik._id);
        return [];
      },
    },
    __proizvod: {
      cellHTMLElement: 'SELECT',
      getValue: (doc) => doc.__proizvod,
      setValue: (
        value: any,
        tableData: { entity: KupacProizvod; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        let noviProizvodId: string = value;
        let noviProizvod: Proizvod = entityRegister.getEntity(noviProizvodId);

        let stariProizvod = tableData.entity.get__proizvod();

        tableData.doc[attrName] = noviProizvodId;
        tableData.entity.set__proizvod(noviProizvod);

        if (stariProizvod)
          stariProizvod.set__proizvodKupci(
            stariProizvod
              .get__proizvodKupci()
              .filter(
                (kupacProizvod) =>
                  kupacProizvod.get_id() !== tableData.entity.get_id()
              )
          );
        noviProizvod.get__proizvodKupci().push(tableData.entity);

        console.log('update', tableData);
        if (tableData.entity)
          CRUDService.put(tableData.entity as any, 'kupacproizvodi');
        if (noviProizvod) CRUDService.put(noviProizvod as any, 'proizvodi');
        if (stariProizvod) CRUDService.put(stariProizvod as any, 'proizvodi');
      },
      getOptions: (data: { [key: string]: any[] }) => {
        if (data) return data['proizvodi'].map((stanica) => stanica._id);
        return [];
      },
    },
  },
  proizvodi: {
    _id: {
      cellHTMLElement: 'INPUT-TEXT',
      getValue: (doc) => doc._id,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = value;
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'proizvodi');
      },
      getOptions: (data) => [],
    },
    naziv: {
      cellHTMLElement: 'INPUT-TEXT',
      getValue: (doc) => doc.naziv,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = value;
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'proizvodi');
      },
      getOptions: (data) => [],
    },
    opis: {
      cellHTMLElement: 'INPUT-TEXT',
      getValue: (doc) => doc.opis,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = value;
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'proizvodi');
      },
      getOptions: (data) => [],
    },
    slika: {
      cellHTMLElement: 'INPUT-TEXT',
      getValue: (doc) => doc.slika,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = value;
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'proizvodi');
      },
      getOptions: (data) => [],
    },
    cena: {
      cellHTMLElement: 'INPUT-NUMBER',
      getValue: (doc) => doc.cena,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {
        tableData.doc[attrName] = +value;
        tableData.entity[attrName] = tableData.doc[attrName];

        console.log('update', tableData);
        CRUDService.put(tableData.entity, 'proizvodi');
      },
      getOptions: (data) => [],
    },
    __proizvodKupci: {
      cellHTMLElement: 'LIST',
      getValue: (doc) => doc.__proizvodKupci,
      setValue: (
        value: any,
        tableData: { entity: any; doc: any },
        attrName: string,
        entityRegister: EntityRegister,
        CRUDService: CRUDService
      ) => {},
      getOptions: (data) => [],
    },
  },
};
