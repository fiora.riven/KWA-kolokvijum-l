export const firstLetterUp = (str: string): string =>
  str.charAt(0).toUpperCase() + str.slice(1);

export const newId = (
  lastId: string,
  idsLastNumberDigitsCount: number = 5
): string => {
  let newId = +lastId.slice(-idsLastNumberDigitsCount) + 1 + '';
  let zerosToAdd: number = idsLastNumberDigitsCount - newId.length;
  return (
    lastId.slice(0, -idsLastNumberDigitsCount) +
    Array.from(Array(zerosToAdd).keys()).reduce((acc, el) => '0' + acc, newId)
  );
};
