import { NgModule } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
import { MatRippleModule } from '@angular/material/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';

const material: any[] = [MatSliderModule, MatRippleModule, MatSnackBarModule];

@NgModule({
  declarations: [],
  imports: [...material],
  exports: [...material],
  entryComponents: [],
})
export class MaterialModule {}
