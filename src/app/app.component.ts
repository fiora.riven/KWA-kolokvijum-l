import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { DOC_RECIPES_TYPE, DOC_RECIPES, ENDPOINTS } from './shared/consts';
import { CRUDService } from './shared/service/crud.service';
import { EntityMapper } from './shared/model/classes/entity-mapper.class';
import { EntityRegister } from './shared/model/classes/entity-register.class';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Putnik } from './shared/model/classes/putnik.class';
import { Karta } from './shared/model/classes/karta.class';
import { Stanica } from './shared/model/classes/stanica.class';
import { newId } from './shared/helper-fns';
import { Kupac } from './shared/model/classes/kupac.class';
import { KupacProizvod } from './shared/model/classes/kupac-proizvod.class';
import { Proizvod } from './shared/model/classes/proizvod.class';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  // public
  public data: { [key: string]: any[] } = {};
  public endpoints: string[] = ENDPOINTS;
  public selectedEndpoint?: string;
  public dataForTable: { entity: any; doc: any }[] = [];
  public docRecipes: DOC_RECIPES_TYPE = DOC_RECIPES;

  public disabled: boolean = false;
  public entityRegister: EntityRegister;

  // private

  // subscriptions
  private _CRUDServiceSubscriptions: Subscription[] = [];

  constructor(
    public CRUDService: CRUDService,
    private _entityMapper: EntityMapper,
    private _cdr: ChangeDetectorRef,
    private _snackBar: MatSnackBar
  ) {}

  public getFullPriceWithDiscount = (tableData: {
    entity: Kupac;
    doc: any;
  }): number => {
    let discount: number = tableData.entity.getProcentualniPopustNaProizvode();
    let kupacproizvodi = this.data['kupacproizvodi'].filter((kupacproizvod) =>
      tableData.entity
        .get__kupacProizvodi()
        .map((_kupacproizvod) => (_kupacproizvod as any)._id)
        .includes(kupacproizvod._id)
    );
    let proizvodi = this.data['proizvodi'].filter((proizvod) =>
      kupacproizvodi
        .map((_kupacproizvod: KupacProizvod) =>
          _kupacproizvod.get__proizvod()?.get_id()
        )
        .includes(proizvod._id)
    );
    let price: number = proizvodi.reduce(
      (acc, el: Proizvod) => acc + el.getCena(),
      0
    );
    let discountedPrice: number =
      discount > 0 ? price - price * (discount / 100) : price;

    // console.log(
    //   tableData,
    //   discount,
    //   kupacproizvodi,
    //   proizvodi,
    //   price,
    //   discountedPrice
    // );
    return discountedPrice;
  };

  public addNewEntity = (): void => {
    if (this.selectedEndpoint) {
      // console.log(this.selectedEndpoint);

      let entity: any;
      if (this.selectedEndpoint === 'putnici') {
        entity = new Putnik();
      } else if (this.selectedEndpoint === 'karte') {
        entity = new Karta();
      } else {
        entity = new Stanica();
      }

      // console.log(
      //   this.data[this.selectedEndpoint][
      //     this.data[this.selectedEndpoint].length - 1
      //   ]
      // );
      entity._id = newId(
        this.data[this.selectedEndpoint][
          this.data[this.selectedEndpoint].length - 1
        ]._id
      );
      // console.log(entity);

      this.CRUDService.post(entity, this.selectedEndpoint);
    }
  };

  public deleteEntity = (entity: any): void => {
    if (this.selectedEndpoint) {
      for (let attrName in this.docRecipes[this.selectedEndpoint])
        if (
          this.docRecipes[this.selectedEndpoint][attrName].cellHTMLElement ===
          'LIST'
        ) {
          // console.log(attrName, entity);
          if (entity[attrName].length > 0) {
            this._snackBar.open(
              'Entitet je referenciran od strane drugih entiteta. Raskacite prvo te veze.',
              undefined,
              { duration: 1000 }
            );
            return;
          }
        }
      console.log('delete');
      this.CRUDService.delete(entity, this.selectedEndpoint);
    }
  };

  public setSelectedEndpoint = (selectedEndpoint: string): void => {
    if (this.selectedEndpoint === selectedEndpoint) {
      this.selectedEndpoint = undefined;
      this.dataForTable = [];
      return;
    }

    this.selectedEndpoint = selectedEndpoint;

    this.dataForTable = this.data[this.selectedEndpoint].map((entity) => ({
      entity: entity,
      doc: this._entityMapper.toDoc(entity),
    }));
    // console.log(this.dataForTable);
  };

  public getDocKeys = (doc: any): string[] => Object.keys(doc);

  ngOnInit(): void {
    this.entityRegister = EntityRegister.getInstance();

    this._CRUDServiceSubscriptions.push(
      this.CRUDService.dataBehaviorSubject$.subscribe((data) => {
        this.data = data;

        if (this.selectedEndpoint) {
          this.dataForTable = this.data[this.selectedEndpoint].map(
            (entity) => ({
              entity: entity,
              doc: this._entityMapper.toDoc(entity),
            })
          );
        }

        // console.log(this.data);
      })
    );
  }

  ngAfterViewInit(): void {}

  ngOnDestroy(): void {
    this._CRUDServiceSubscriptions.forEach((subscription) =>
      subscription.unsubscribe()
    );
  }
}
